package org.aossie.agoraandroid.data.network.responses

data class AuthToken(
  var token: String?,
  var expiresOn: String?
)