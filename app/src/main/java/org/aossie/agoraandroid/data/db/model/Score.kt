package org.aossie.agoraandroid.data.db.model

data class Score(
  var numerator: String? = null,
  var denominator: String? = null
)